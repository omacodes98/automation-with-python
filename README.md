# Automation with Python 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)


## Project Description

An application that reads a spreadsheet file and process and manipulate the spreadsheet 

## Technologies Used 

* Python 

* Pycharm 

* Git 

## Steps 

Step 1: Move inventory file into python project directory 

[images](/images/01_move_inventory_file_into_python_project_directory.png)

Step 2: Install openpyxl library to read excel file 

    pip install openpyxl

[images](/images/02_install_openpyxl_library_to_read_excel_file.png)

Step 3: Create main.py file in project directory 

    touch main.py 

[images](/images/03_create_main_py_file_in_project_directory.png)

Step 4: import openpyxl now its available locally 

[images](/images/04_import_openpyxl_now_its_available_locally.png)

Step 5: Use function from openpyxl library called load workbook and insert file name as a parameter this will read spread sheet file

[images](/images/05_use_function_from_openpyxl_library_called_load_workbook_and_insert_file_name_as_parameter_this_will_read_spread_sheet_file.png)

Step 6: Save it as a variable 

[images](/images/06_save_it_as_a_variable.png)

Step 7: Check the name of the sheet you want the program to read 

[images](/images/07_check_the_name_of_sheet_you_want_the_program_to_read.png)

Step 8: Insert the name of the sheet you want to use with appropriate syntax and save it into a variable  

[images](/images/08_insert_the_name_of_the_sheet_you_want_to_use_with_appropriate_syntax_and_save_it_into_a_variable.png)

Step 9: Create a variable that will store the result product per supplier 

[images](/images/09_create_a_variable_that_will_store_the_result_products_per_supplier.png)

Step 10: Add for loop that enables program to loop over every product and get company names

[images](/images/10_add_for_loop_that_enable_program_to_loop_over_every_product_and_get_company_name.png)

Step 11: Add two as a first parameter so the loop can start at row two, this is because row one just has the name 

[images](/images/11_add_two_as_a_first_parameter_so_the_loop_can_start_at_row_two_this_is_becaue_row_one_just_has_the_names.png)

Step 12: Add another parameter plus one this is because the second number exclusive and we want to include last role

[images](/images/12_add_another_parameter_plus_one_this_is_because_the_second_number_plmr_is_exclusive_stop_specified_no_not_included_so_we_want_to_include_the_last_role.png)

Step 13: Add logic to enable program loop to find supplier name and save in variable this will enable loop to have access to 4 colone which has the name of suppliers

[images](/images/13_add_logic_to_enable_program_loop_to_find_supplier_name_this_and_save_in_variable_this_will_enable__loop_to_have_access_to_4_colone_which_has_the_names_of_suppliers.png)

Step 14: Add logic to add product count when we find another product for the same supplier 

[images](/images/14_add_logic_to_add_product_count_when_we_find_another_product_for_the_same_supplier.png)

Step 15: Test program

[images](/images/15_test_program.png)

Step 16: Calculate total inventory per supplier start by creating an empty dictionary

[images](/images/16_next_step_calculate_total_vlaue_of_inventory_per_supplier_star_by_creating_an_empty_dictionary.png)

Step 17: Add logic to calculate total inventory per supplier this will check if the supply value exists and if it does add oon to the number if it doesnt it will just add the new value 

[images](/images/17_add_logic_to_calculate_total_inventory_per_supply_this_will_check_if_supply_value_exists_and_if_it_does_add_on_to_the_number_if_it_doesnt_it_will_just_add_the_new_value.png)

Step 18: Add logic to enable program loop dto find suppliers inventory and price 

[images](/images/17_add_logic_to_enable_program_loop_to_find_suppliers_inventory_and_price.png)

Step 19: Test Program 

[test program](/images/18_test_program_again.png)

Step 20: Add product number to the for loop 

[product no loop](/images/19_add_product_number_to_the_for_loop.png)

Step 21: Add if else statememnt logic to add product numbers with inventory less than 10

[less than 10](/images/20_add_if_else_statement_logic_to_add_product_numbers_with_inventory_less_than_10.png)

Add loop to access empty 5th role 

[5th role access](/images/21_add_loop_to_access_empty_5th_role.png)

Add inventory x price in the loop so it can calculate inventory price for everyy row 

[inv x price](/images/22_add_inv_x_price_in_the_loop_so_it_can_calculate_inventory_price_for_every_row.png)

## Installation

    brew install python 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/automation-with-python.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/automation-with-python

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.

