import openpyxl

inv_file = openpyxl.load_workbook("inventory.xlsx")
product_list = inv_file["Sheet1"]

'''First task calculate how many products we have per supplier
 and list the names of suppliers with the number of product'''

total_value_per_supplier = {}
products_per_supplier = {}
products_under_10_inv = {}

''' Firstly we need to go through each of the roles in order
in order and we need to check a supplier name for that role
and we need to check a supplier name for that role 
this means we need to use a loop
we need to tell the loop how many times it should execute the logic
we have 74 products so we need to do that 74 times for each product '''

for product_row in range(2, product_list.max_row + 1):
    supply_name = product_list.cell(product_row, 4).value
    inventory = product_list.cell(product_row, 2).value
    price = product_list.cell(product_row, 3).value
    product_number = product_list.cell(product_row, 1).value
    inventory_price = product_list.cell(product_row, 5)

    if supply_name in products_per_supplier:
        current_num_products = products_per_supplier[supply_name]
        products_per_supplier[supply_name] = current_num_products + 1

    else:
        print("adding a new supplier")
        products_per_supplier[supply_name] = 1


    ''' calculate the total value of inventory per supplier'''

    if supply_name in total_value_per_supplier:
        current_total_value = total_value_per_supplier[supply_name]
        total_value_per_supplier[supply_name] = current_total_value + inventory * price
    else:
       total_value_per_supplier[supply_name] = inventory + price

    ''' products with inventory less than then 10'''
    if inventory < 10:
        products_under_10_inv[product_number] = inventory

    ''' add value for total inventory price'''

    inventory_price.value = inventory * price

print(total_value_per_supplier)
print(products_per_supplier)
print(products_under_10_inv)

inv_file.save("updated_file.xlsx")

